const express = require('express');
var faker = require('faker');
const router = express.Router();

/* GET users listing. */
router.get('/', function (req, res) {
    res.json([
      {
          name: faker.name.findName(),
          email: faker.internet.email(),
          image: faker.image.avatar()
      },
      {
          name: faker.name.findName(),
          email: faker.internet.email(),
          image: faker.image.avatar()
      },
      {
          name: faker.name.findName(),
          email: faker.internet.email(),
          image: faker.image.avatar()
      },
    ]);
});
router.get('/:userid', function (req, res) {
    res.json({
        name: faker.name.findName(),
        email: faker.internet.email(),
        address: faker.address.streetAddress(),
        image: faker.image.avatar()
    });
});
router.get('/:userid/messages', function (req, res) {
    res.json([
       {
           name: faker.name.findName(),
           message: faker.lorem.sentence()
       },
       {
           name: faker.name.findName(),
           message: faker.lorem.sentence()
       },
       {
           name: faker.name.findName(),
           message: faker.lorem.sentence()
       },
    ]);;
});
module.exports = router;
