const express = require('express');
var faker = require('faker');
const router = express.Router();

/* GET users listing. */
router.get('/', function (req, res) {
    res.json([
    {
        name: faker.lorem.word(),
        place: faker.address.streetAddress(),
    },
    {
        name: faker.lorem.word(),
        place: faker.address.streetAddress(),
    },
    {
        name: faker.lorem.word(),
        place: faker.address.streetAddress(),
    },
    ]);
});
router.get('/:eventid', function (req, res) {
    res.json([
    {
        name: faker.name.findName(),
        email: faker.internet.email(),
        image: faker.image.avatar()
    },
    {
        name: faker.name.findName(),
        email: faker.internet.email(),
        image: faker.image.avatar()
    },
    {
        name: faker.name.findName(),
        email: faker.internet.email(),
        image: faker.image.avatar()
    },
    ]);
});
module.exports = router;
