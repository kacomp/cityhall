<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:template match="/">
        <html>
            <body>
                <div itemscope="" itemtype="schema.org/Event">
                    <h1>Person</h1>
                    <div>i <xsl:value-of select="info//@image"/></div>
                    <h2>Name: <xsl:value-of select="info//@name"/></h2>
                    <h3>id: <xsl:value-of select="info//@id"/></h3>
                    <h3>Friends: <xsl:value-of select="info//@friends"/></h3>
                    <h3>Events: <xsl:value-of select="info//@events"/></h3>
                </div>
            </body>
        </html>
    </xsl:template>
</xsl:stylesheet>