<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:template match="/">
        <html>
            <body>
                <div itemscope="" itemtype="schema.org/Event">
                    <h1>Messages</h1>
                    <h2>Count of messages <xsl:value-of select="count(//message)"/></h2>
                    <xsl:apply-templates select="//message"/>
                </div>
            </body>
        </html>
    </xsl:template>
    <xsl:template match="message">
        <table border="1">
            <tr>
                <td itemprop="name"><xsl:value-of select="@personName"/></td>
                <td itemprop="image"><xsl:value-of select="@text"/></td>
            </tr>
        </table>
    </xsl:template>
</xsl:stylesheet>