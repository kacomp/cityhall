<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:template match="/">
        <html>
            <body>
                <div itemscope="" itemtype="schema.org/Event">
                    <h1>Near events in Minsk</h1>
                    <h2>Count of events <xsl:value-of select="count(//event)"/></h2>
                    <xsl:apply-templates select="//event"/>
                </div>
            </body>
        </html>
    </xsl:template>
    <xsl:template match="event">
        <xsl:if test="generate-id(.)=generate-id(//event[@place=current()/@place][1])">
            <h4 itemprop="location">Events in <xsl:value-of select="@place"/></h4>
            <table border="1">
                <xsl:apply-templates mode="local" select="//event[@place=current()/@place]"/>
            </table>
        </xsl:if>
    </xsl:template>
    <xsl:template match="event" mode="local">
        <tr>
            <td itemprop="nameEvent"><xsl:value-of select="@name"/></td>
            <td itemprop="adress"><xsl:value-of select="@place"/></td>
            <td itemprop="date"> <xsl:value-of select="@date"/></td>
            <td itemprop="price"><xsl:value-of select="@price"/></td>
        </tr>
    </xsl:template>
</xsl:stylesheet>