<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:template match="/">
        <html>
            <body>
                <div itemscope="" itemtype="schema.org/Event">
                    <h1>Persons at this event</h1>
                    <h2>Count of persons <xsl:value-of select="count(//person)"/></h2>
                    <xsl:apply-templates select="//person"/>
                </div>
            </body>
        </html>
    </xsl:template>
    <xsl:template match="person">
            <table border="1">
                <tr>
                    <td itemprop="name"><xsl:value-of select="@name"/></td>
                    <td itemprop="image"><xsl:value-of select="@image"/></td>
                </tr>
            </table>
    </xsl:template>
</xsl:stylesheet>